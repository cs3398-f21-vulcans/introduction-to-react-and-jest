import React from 'react';
import {calculateWinner} from '../calculateWinner.js'

/*

	Winning tic tac toe squares (when each square has same symbol)
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
 */

describe('X Winner Tests', ()=>{
    //  testsquare indices = [0,1,2,3,4,5,6,7,8]
    const testsquares = [null,null,null,null,null,null,null,null];
    test('Expect Winner to be X, left-to-right diagonal down', () => {
        testsquares[0] = 'X';
        testsquares[4] = 'X';
        testsquares[8] = 'X';
        expect(calculateWinner(testsquares)).toBe('X');
    });

    test('Expect Winner to be X, right-to-left diagonal up', () => {
        testsquares[2] = 'X';
        testsquares[4] = 'X';
        testsquares[6] = 'X';
        expect(calculateWinner(testsquares)).toBe('X');
    });
});

describe('O Winner Tests', ()=>{
    //  testsquare indices = [0,1,2,3,4,5,6,7,8]
    const testsquares = [null,null,null,null,null,null,null,null];
    test('Expect Winner to be O, middle horizontal', () => {
        testsquares[3] = 'O';
        testsquares[4] = 'O';
        testsquares[5] = 'O';
        expect(calculateWinner(testsquares)).toBe('O');
    });

    test('Expect Winner to be O, middle vertical', () => {
        testsquares[1] = 'O';
        testsquares[4] = 'O';
        testsquares[7] = 'O';
        expect(calculateWinner(testsquares)).toBe('O');
    });
});
